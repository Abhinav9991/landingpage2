const hamburgerEl = document.getElementById('hamburger'); 
const mobileNavEl = document.querySelector('.mobile-nav'); 
const mainEl = document.querySelector('main'); 
let isClicked = false; 

hamburgerEl.addEventListener('click', () => {
    isClicked = !isClicked; 
    if(isClicked) {
        hamburgerEl.src = './images/icon-close.svg'; 
        mobileNavEl.style.display = "flex"; 
        mainEl.style.opacity = "0.5"; 
    }
    else {
        hamburgerEl.src = './images/icon-hamburger.svg'; 
        mobileNavEl.style.display = "none";
        mainEl.style.opacity = "1"; 
    }
}); 